# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)


# Instalaciones:

## `yarn add react-router-dom`
## `yarn add web3-react`
## `yarn add @web3-react/core web3-react`
## `yarn add @chakra-ui/react @chakra-ui/icons @emotion/react @emotion/styled framer-motion@^4.1.17`
## `yarn add @web3-react/injected-connector`


# DEPLOY IPFS Infura:

## yarn build
## sh deploy.sh

https://ipfs.github.io/public-gateway-checker/

{"Name":"favicon.ico","Hash":"bafkreib5cd35u3dagf4diaebm2gevrntv2luhsu2eyvlb7gtcl53t5el3u","Size":"3870"}
{"Name":"images/platzi.svg","Hash":"bafkreiatlzv65lmb6r6oetsypd4f4i2uu6ot2paoljjti6dlgb243r65oe","Size":"2377"}
{"Name":"index.html","Hash":"bafkreid6r4owk2mhtbe6b6phg7qknbbfdrosrr4z2w6a2wljcs6gerdene","Size":"2185"}
{"Name":"logo192.png","Hash":"bafkreigdqy4w5rynwnqia5nv7p5kysvrzsvinoqfu2flhe7mkuplm3b6aa","Size":"5347"}
{"Name":"logo512.png","Hash":"bafkreie6ut2nu4cqydgebcjg62rzyjjwetu3voy5ipdzo7gyefcfuyfume","Size":"9664"}
{"Name":"manifest.json","Hash":"bafkreicqwpmmheb26p3y3by3srkxvmkpjy44ugjovsr5ft5impegoj42cq","Size":"492"}
{"Name":"robots.txt","Hash":"bafkreieq2jf4hp3jrla6c43tsubctdgmu4vn6h2wj6vql5eexdci2hfn2i","Size":"67"}
{"Name":"static/js/2.a8a11b67.chunk.js","Hash":"bafybeiacdx2m7bsavlele7fa6ojavbnzywogqu3acnoxoy7mkdqdm646sy","Size":"3347209"}
{"Name":"static/js/2.a8a11b67.chunk.js.LICENSE.txt","Hash":"bafkreidehatqxq6zgcgeyu4eghjwmqinlliqixi2dal7upjdmg7ltiieui","Size":"2725"}
{"Name":"static/js/2.a8a11b67.chunk.js.map","Hash":"bafybeia4iss7fn6jbjkwn6g4iikmgtf6lk7ktfoz2m2iwvvpgbhgsacavq","Size":"9425104"}
{"Name":"static/js/main.9eeacc12.chunk.js","Hash":"bafkreif2ciywgo2ymdadaldma3qaoyxl4kwj6yixa55wwywxnbedr2scvm","Size":"23523"}
{"Name":"static/js/main.9eeacc12.chunk.js.map","Hash":"bafkreibn62mrrdqrfrfuik2fia5uc53gnhwm7fajz25gh655bckro2qply","Size":"70841"}
{"Name":"static/js/runtime-main.1066ef45.js","Hash":"bafkreiddbaqttpabwiterglwqnuyzc36m7s3xj3rnvv4ly6paftwz2adfu","Size":"1591"}
{"Name":"static/js/runtime-main.1066ef45.js.map","Hash":"bafkreia6uhtgbond2elzkha2ndwu5dkt4p6tw3nlrr5ofnmxelzk2sykle","Size":"8295"}
{"Name":"images","Hash":"bafybeibhjqp6fqv5qbfokr2tv44xgcudhmnh746seo7e4mtq4f364dd5py","Size":"2436"}
{"Name":"static/js","Hash":"bafybeig6wysmpemenwte5y2dbo7urxfqr64lgn76xlp4ffol2kilfl2fda","Size":"12879786"}
{"Name":"static","Hash":"bafybeif6l3rd7fcvi6sbmuwpcqadvwdw2kkr752h6nf3fcx6q6lccyadey","Size":"12879839"}
{"Name":"","Hash":"bafybeia6hsnjw776fr6xqou2ytwveo5z3t7lbfog66zjmey6fbg4ex34dq","Size":"12905090"}

https://bafybeia6hsnjw776fr6xqou2ytwveo5z3t7lbfog66zjmey6fbg4ex34dq.ipfs.dweb.link/
